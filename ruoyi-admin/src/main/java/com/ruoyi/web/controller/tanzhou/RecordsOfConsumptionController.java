package com.ruoyi.web.controller.tanzhou;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.tanzhou.domain.RecordsOfConsumption;
import com.ruoyi.tanzhou.dto.RecordsOfConsumptionDto;
import com.ruoyi.tanzhou.service.IRecordsOfConsumptionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 消费记录Controller
 *
 * @author ruoyi
 * @date 2020-04-24
 */
@Controller
@RequestMapping("/tanzhou/consumption")
public class RecordsOfConsumptionController extends BaseController {
    private String prefix = "tanzhou/consumption";

    @Autowired
    private IRecordsOfConsumptionService recordsOfConsumptionService;

    @RequiresPermissions("tanzhou:consumption:view")
    @GetMapping()
    public String consumption() {
        return prefix + "/consumption";
    }

    /**
     * 查询消费记录列表
     */
    @RequiresPermissions("tanzhou:consumption:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RecordsOfConsumption recordsOfConsumption) {
        startPage();
        List<RecordsOfConsumption> list = recordsOfConsumptionService.selectRecordsOfConsumptionList(recordsOfConsumption);
        return getDataTable(list);
    }

    /**
     * 导出消费记录列表
     */
    @RequiresPermissions("tanzhou:consumption:export")
    @Log(title = "消费记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RecordsOfConsumption recordsOfConsumption) {
        List<RecordsOfConsumption> list = recordsOfConsumptionService.selectRecordsOfConsumptionList(recordsOfConsumption);
        ExcelUtil<RecordsOfConsumption> util = new ExcelUtil<RecordsOfConsumption>(RecordsOfConsumption.class);
        return util.exportExcel(list, "consumption");
    }

    /**
     * 新增消费记录
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存消费记录
     */
    @RequiresPermissions("tanzhou:consumption:add")
    @Log(title = "消费记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RecordsOfConsumption recordsOfConsumption) {
        return toAjax(recordsOfConsumptionService.insertRecordsOfConsumption(recordsOfConsumption));
    }

    /**
     * 修改消费记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {
        RecordsOfConsumption recordsOfConsumption = recordsOfConsumptionService.selectRecordsOfConsumptionById(id);
        mmap.put("recordsOfConsumption", recordsOfConsumption);
        return prefix + "/edit";
    }

    /**
     * 修改保存消费记录
     */
    @RequiresPermissions("tanzhou:consumption:edit")
    @Log(title = "消费记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RecordsOfConsumption recordsOfConsumption) {
        return toAjax(recordsOfConsumptionService.updateRecordsOfConsumption(recordsOfConsumption));
    }

    /**
     * 删除消费记录
     */
    @RequiresPermissions("tanzhou:consumption:remove")
    @Log(title = "消费记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(recordsOfConsumptionService.deleteRecordsOfConsumptionByIds(ids));
    }


    /**
     * 导入后台用户列表
     */
    @Log(title = "消费记录", businessType = BusinessType.IMPORT)
    @RequiresPermissions("tanzhou:consumption:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<RecordsOfConsumptionDto> util = new ExcelUtil<>(RecordsOfConsumptionDto.class);
        List<RecordsOfConsumptionDto> enterpriseList = util.importExcel(file.getInputStream());
        String operatorName = ShiroUtils.getSysUser().getLoginName();
        new Thread(() -> recordsOfConsumptionService.importData(enterpriseList, updateSupport, operatorName)).start();

        return AjaxResult.success("已经导入，正在处理，请稍后再来查看");
    }


    @RequiresPermissions("tanzhou:consumption:view")
    @GetMapping("/business")
    public String business() {
        return prefix + "/business";
    }
}
