package com.ruoyi.tanzhou.service;

import com.ruoyi.tanzhou.domain.RecordsOfConsumption;
import com.ruoyi.tanzhou.dto.RecordsOfConsumptionDto;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消费记录Service接口
 *
 * @author ruoyi
 * @date 2020-04-24
 */
public interface IRecordsOfConsumptionService {
    /**
     * 查询消费记录
     *
     * @param id 消费记录ID
     * @return 消费记录
     */
    public RecordsOfConsumption selectRecordsOfConsumptionById(String id);

    /**
     * 查询消费记录列表
     *
     * @param recordsOfConsumption 消费记录
     * @return 消费记录集合
     */
    public List<RecordsOfConsumption> selectRecordsOfConsumptionList(RecordsOfConsumption recordsOfConsumption);

    /**
     * 新增消费记录
     *
     * @param recordsOfConsumption 消费记录
     * @return 结果
     */
    public int insertRecordsOfConsumption(RecordsOfConsumption recordsOfConsumption);

    /**
     * 修改消费记录
     *
     * @param recordsOfConsumption 消费记录
     * @return 结果
     */
    public int updateRecordsOfConsumption(RecordsOfConsumption recordsOfConsumption);

    /**
     * 批量删除消费记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRecordsOfConsumptionByIds(String ids);

    /**
     * 删除消费记录信息
     *
     * @param id 消费记录ID
     * @return 结果
     */
    public int deleteRecordsOfConsumptionById(String id);

    void importData(List<RecordsOfConsumptionDto> enterpriseList, boolean updateSupport, String operatorName);


    public List<RecordsOfConsumption> selectAllStateStr();
}
