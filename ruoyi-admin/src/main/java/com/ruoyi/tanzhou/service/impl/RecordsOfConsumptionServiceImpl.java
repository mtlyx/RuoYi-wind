package com.ruoyi.tanzhou.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.UUIDUtils;
import com.ruoyi.tanzhou.domain.RecordsOfConsumption;
import com.ruoyi.tanzhou.dto.RecordsOfConsumptionDto;
import com.ruoyi.tanzhou.mapper.RecordsOfConsumptionMapper;
import com.ruoyi.tanzhou.service.IRecordsOfConsumptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.UUIDEditor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.Transient;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 消费记录Service业务层处理
 *
 * @author ruoyi
 * @date 2020-04-24
 */
@Service("recordOfConsumption")
public class RecordsOfConsumptionServiceImpl implements IRecordsOfConsumptionService {
    @Resource
    private RecordsOfConsumptionMapper recordsOfConsumptionMapper;

    private List<RecordsOfConsumption> stateList;
    private Long updateStateTime;


    /**
     * 查询消费记录
     *
     * @param id 消费记录ID
     * @return 消费记录
     */
    @Override
    public RecordsOfConsumption selectRecordsOfConsumptionById(String id) {
        return recordsOfConsumptionMapper.selectRecordsOfConsumptionById(id);
    }

    /**
     * 查询消费记录列表
     *
     * @param recordsOfConsumption 消费记录
     * @return 消费记录
     */
    @Override
    public List<RecordsOfConsumption> selectRecordsOfConsumptionList(RecordsOfConsumption recordsOfConsumption) {
        return recordsOfConsumptionMapper.selectRecordsOfConsumptionList(recordsOfConsumption);
    }

    /**
     * 新增消费记录
     *
     * @param recordsOfConsumption 消费记录
     * @return 结果
     */
    @Override
    public int insertRecordsOfConsumption(RecordsOfConsumption recordsOfConsumption) {
        recordsOfConsumption.setCreateTime(DateUtils.getNowDate());
        return recordsOfConsumptionMapper.insertRecordsOfConsumption(recordsOfConsumption);
    }

    /**
     * 修改消费记录
     *
     * @param recordsOfConsumption 消费记录
     * @return 结果
     */
    @Override
    public int updateRecordsOfConsumption(RecordsOfConsumption recordsOfConsumption) {
        return recordsOfConsumptionMapper.updateRecordsOfConsumption(recordsOfConsumption);
    }

    /**
     * 删除消费记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecordsOfConsumptionByIds(String ids) {
        return recordsOfConsumptionMapper.deleteRecordsOfConsumptionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除消费记录信息
     *
     * @param id 消费记录ID
     * @return 结果
     */
    @Override
    public int deleteRecordsOfConsumptionById(String id) {
        return recordsOfConsumptionMapper.deleteRecordsOfConsumptionById(id);
    }

    @Override
    @Transient
    public void importData(List<RecordsOfConsumptionDto> enterpriseList, boolean updateSupport, String operatorName) {
        //先过滤无效的字段
        enterpriseList = enterpriseList.stream().filter(item -> !item.checkData()).collect(Collectors.toList());

        enterpriseList.forEach(item -> {
            //先检测是否有之前的记录
            RecordsOfConsumption record = new RecordsOfConsumption();
            record.setUserName(item.getUserName());
            record.setShopName(item.getShopName());
            record.setPhone(item.getPhone());
            record.setOrderTime(item.getOrderTime());
            List<RecordsOfConsumption> beforeList = recordsOfConsumptionMapper.selectRecordsOfConsumptionList(record);
            if (beforeList.isEmpty()) {
                record.setId(UUIDUtils.getUUID());
                BeanUtils.copyProperties(item, record);
                recordsOfConsumptionMapper.insertRecordsOfConsumption(record);
            } else {
                record.setId(beforeList.get(0).getId());
                BeanUtils.copyProperties(item, record);
                recordsOfConsumptionMapper.updateRecordsOfConsumption(record);
            }
        });
    }

    @Override
    public List<RecordsOfConsumption> selectAllStateStr() {
        if (this.stateList == null || this.updateStateTime == null) {
            this.stateList = recordsOfConsumptionMapper.selectAllStateStr();
            this.updateStateTime = System.currentTimeMillis();
        } else if (System.currentTimeMillis() - this.updateStateTime > 60 * 1000) {
            this.stateList = recordsOfConsumptionMapper.selectAllStateStr();
            this.updateStateTime = System.currentTimeMillis();
        }

        return this.stateList;
    }
}
