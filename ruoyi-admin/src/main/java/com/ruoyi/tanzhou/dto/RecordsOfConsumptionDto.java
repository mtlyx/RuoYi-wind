package com.ruoyi.tanzhou.dto;


import com.google.common.base.Strings;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class RecordsOfConsumptionDto {
    @Excel(name = "客户姓名")
    private String userName;
    @Excel(name = "手机号")
    private String phone;

    @Excel(name = "报名时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm")
    private Date orderTime;

    @Excel(name = "参与门店")
    private String shopName;

    /**
     * 消费金额
     */
    @Excel(name = "消费金额")
    private Double consumptionAmount;

    /**
     * 补贴金额
     */
    @Excel(name = "补贴金额")
    private Double subsidyAmount;

    /**
     * 票据存储地址
     */
    @Excel(name = "申请票据")
    private String billAddress;

    /**
     * 状态
     */
    @Excel(name = "审核状态")
    private String stateStr;

    /**
     * 微信付款订单号
     */
    @Excel(name = "微信付款单号")
    private String weixinPaymentOrderNo;

    /**
     * 付款时间
     */
    @Excel(name = "付款成功时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm")
    private Date paymentTime;


    public boolean checkData() {
        return Strings.isNullOrEmpty(this.userName)
                || Strings.isNullOrEmpty(this.phone)
                || Strings.isNullOrEmpty(this.shopName)
                || Strings.isNullOrEmpty(this.weixinPaymentOrderNo)
                ;

    }
}
