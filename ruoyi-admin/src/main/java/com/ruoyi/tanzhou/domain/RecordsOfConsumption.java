package com.ruoyi.tanzhou.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 消费记录对象 records_of_consumption
 * 
 * @author ruoyi
 * @date 2020-04-24
 */
public class RecordsOfConsumption extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 报名时间 */
    @Excel(name = "报名时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 电话号码 */
    @Excel(name = "电话号码")
    private String phone;

    /** 商家名 */
    @Excel(name = "商家名")
    private String shopName;

    /** 消费金额 */
    @Excel(name = "消费金额")
    private Double consumptionAmount;

    /** 补贴金额 */
    @Excel(name = "补贴金额")
    private Double subsidyAmount;

    /** 票据存储地址 */
    @Excel(name = "票据存储地址")
    private String billAddress;

    /** 状态 */
    @Excel(name = "状态")
    private String stateStr;

    /** 微信付款订单号 */
    @Excel(name = "微信付款订单号")
    private String weixinPaymentOrderNo;

    /** 付款时间 */
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentTime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setOrderTime(Date orderTime) 
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime() 
    {
        return orderTime;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setShopName(String shopName) 
    {
        this.shopName = shopName;
    }

    public String getShopName() 
    {
        return shopName;
    }
    public void setConsumptionAmount(Double consumptionAmount) 
    {
        this.consumptionAmount = consumptionAmount;
    }

    public Double getConsumptionAmount() 
    {
        return consumptionAmount;
    }
    public void setSubsidyAmount(Double subsidyAmount) 
    {
        this.subsidyAmount = subsidyAmount;
    }

    public Double getSubsidyAmount() 
    {
        return subsidyAmount;
    }
    public void setBillAddress(String billAddress) 
    {
        this.billAddress = billAddress;
    }

    public String getBillAddress() 
    {
        return billAddress;
    }
    public void setStateStr(String stateStr) 
    {
        this.stateStr = stateStr;
    }

    public String getStateStr() 
    {
        return stateStr;
    }
    public void setWeixinPaymentOrderNo(String weixinPaymentOrderNo) 
    {
        this.weixinPaymentOrderNo = weixinPaymentOrderNo;
    }

    public String getWeixinPaymentOrderNo() 
    {
        return weixinPaymentOrderNo;
    }
    public void setPaymentTime(Date paymentTime) 
    {
        this.paymentTime = paymentTime;
    }

    public Date getPaymentTime() 
    {
        return paymentTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createTime", getCreateTime())
            .append("orderTime", getOrderTime())
            .append("userName", getUserName())
            .append("phone", getPhone())
            .append("shopName", getShopName())
            .append("consumptionAmount", getConsumptionAmount())
            .append("subsidyAmount", getSubsidyAmount())
            .append("billAddress", getBillAddress())
            .append("stateStr", getStateStr())
            .append("weixinPaymentOrderNo", getWeixinPaymentOrderNo())
            .append("paymentTime", getPaymentTime())
            .toString();
    }
}
